## Network Speed Test

Check the download speed for the current network. Intended to run headless, perhaps triggered with a button or at boot via 

### Installation

Download from git:

`git clone https://pangolinpaw@bitbucket.org/pangolinpaw/network_test.git`

Navigate to containing folder:

`cd network_test`

Install requirements:

`pip3 install -r requirements.txt`

### Usage

```
usage: python3 speedtest.py [-h] recipient key

positional arguments:
  recipient   Email address to which report is sent
  key         API key

optional arguments:
  -h, --help  show this help message and exit
```

#### Trigger from pisugar

Under Custom Button Function, select Custom Shell from one of the drop-downs and enter the following command:

```python3 /full/path/to/speedtest.py [your-email@address.com] [your_api_key]```

