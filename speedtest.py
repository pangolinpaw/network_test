import datetime
import time
import csv
import os
import argparse
import requests

dirname, scriptname = os.path.split(os.path.abspath(__file__))
SAVE_AS = f'{dirname}{os.sep}download_tests.csv'

def estimate_speed(size, duration):
    speed = size / duration
    return speed

def parse_time(duration):
    duration = time.strptime(str(duration).split('.')[0],'%H:%M:%S')
    duration = datetime.timedelta(
        hours=duration.tm_hour,
        minutes=duration.tm_min,
        seconds=duration.tm_sec
    ).total_seconds()
    return duration

def download(size=50, verbose=False):
    start = datetime.datetime.now()
    if verbose:
        print(f'Download started at {start}')
    r = requests.get(f'http://ipv4.download.thinkbroadband.com/{size}MB.zip')
    duration = datetime.datetime.now() - start
    duration_seconds = int(parse_time(duration))
    if verbose:
        print(f'Download finished. Elapsed time: {duration_seconds} seconds')
    return duration_seconds

def save_csv(filename, data):
    with open(filename, 'a', newline='', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerows(data)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('recipient', help='Email address to which report is sent')
    parser.add_argument('key', help='API key')
    args = parser.parse_args()

    times = []
    for x in range(3):
        print(f'Test #{x +1}')
        size = 20
        elapsed_time = download(size)
        times.append(elapsed_time)
        download_speed = round(estimate_speed(size, elapsed_time), 2)
        print(f'    Time: {elapsed_time} seconds ({download_speed} mb/sec)')
        if not os.path.exists(SAVE_AS):
            data = [
                ['timestamp', 'size', 'time', 'speed'],
                [datetime.datetime.now().strftime('%Y-%m-%d %H:%M'), size, elapsed_time, download_speed]
            ]
        else:
            data = [[datetime.datetime.now().strftime('%Y-%m-%d %H:%M'), size, elapsed_time, download_speed]]
        save_csv(SAVE_AS, data)

    average_time = round(sum(times) / len(times), 1)
    print(f'Average: {average_time} seconds ({size / average_time} mb/sec)')


    subject = 'Raspberry Pi network test'
    body = f'Network test completed at {datetime.datetime.now().strftime("%Y-%m-%d %H:%M")}\n\nFile size: {size}mb\nRepeats: {len(times)}\nAverage time taken: {average_time} seconds'
    requests.get(f"http://pangolinpaw.eu.pythonanywhere.com/mail?recipient={args.recipient}&subject={subject}&body={body}&key={args.key}")


if __name__ == '__main__':
    main()
